//
//  RocketCollectionViewCell.swift
//  TappableSpaceX
//
//  Created by Stuart Thomas on 21/01/2021.
//

import UIKit

class RocketTableViewCell: UITableViewCell {
  
  @IBOutlet weak var rocketImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var successRateLabel: UILabel!
  @IBOutlet weak var successRateImageView: UIImageView!
  @IBOutlet weak var dateFirstFlightLabel: UILabel!
  
  var rocket : RocketViewModel? {
    didSet {
      rocketImageView.image = rocket?.mainImage
      nameLabel.text = rocket?.name
      successRateLabel.text = "Success Rate - "
      
      dateFirstFlightLabel.text = rocket?.firstFlight
    }
  }
}
