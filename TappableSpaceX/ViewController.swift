//
//  ViewController.swift
//  TappableSpaceX
//
//  Created by Stuart Thomas on 20/01/2021.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  let viewModel = RocketViewModel(dataService: DataService())
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    attemptFetchRockets()
  }
  private func attemptFetchRockets() {
    viewModel.fetchRockets()
    
    viewModel.changeLoadingStatus = {
      let _ = self.viewModel.isLoading ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
    }
    
    viewModel.showAlert = {
      if let error = self.viewModel.error {
        print(error.localizedDescription)
        self.showErrorAlert()
      }
    }
    
    viewModel.didFinishFetch = {
      self.tableView.isHidden = false
      self.tableView.reloadData()
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.formattedRockets.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! RocketTableViewCell
    cell.nameLabel.text = viewModel.formattedRockets[indexPath.row].name
    cell.rocketImageView.image = viewModel.formattedRockets[indexPath.row].mainImage
    cell.successRateLabel.text = viewModel.formattedRockets[indexPath.row].successRateText
    cell.successRateImageView.image = viewModel.formattedRockets[indexPath.row].successRateBadge
    cell.dateFirstFlightLabel.text = viewModel.formattedRockets[indexPath.row].firstFlight
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    guard let detailsViewController = storyBoard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController else { return }
    detailsViewController.selectedRocket = viewModel.formattedRockets[indexPath.row]
    self.navigationController?.pushViewController(detailsViewController,
                                                  animated: true)
  }
  
  func showErrorAlert() {
    let alert = createAlert()
    self.present(alert, animated: true, completion: nil)
  }
  
  func handleTryAgain() {
    attemptFetchRockets()
  }
  
  func handleLoadCachedImages() {
    let coreDataHelper = CoreDataHelper()
    guard let formattedRockets = coreDataHelper.retrieve() else { return }
    viewModel.formattedRockets = formattedRockets
    self.tableView.reloadData()
    self.tableView.isHidden = false
  }
  
  private func createAlert() -> UIAlertController {
    let message = "Could not connect to API."
    let alert = UIAlertController(title: message,
                                  message: nil,
                                  preferredStyle: UIAlertController.Style.alert)
    addTryAgainAction(alert: alert)
    addLoadCachedImagesAction(alert: alert)
    return alert
  }
  
  private func addTryAgainAction(alert: UIAlertController) {
    alert.addAction(UIAlertAction(title: "Try Again",
                                  style: UIAlertAction.Style.default,
                                  handler: { (action: UIAlertAction) in
                                    self.handleTryAgain()
                                  }))
  }
  
  private func addLoadCachedImagesAction(alert: UIAlertController) {
    alert.addAction(UIAlertAction(title: "Load Cached Images",
                                  style: UIAlertAction.Style.default,
                                  handler: { (action: UIAlertAction) in
                                    self.handleLoadCachedImages()
                                  }))
  }
}

