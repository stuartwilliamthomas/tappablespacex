//
//  DetailsViewController.swift
//  TappableSpaceX
//
//  Created by Stuart Thomas on 24/01/2021.
//

import UIKit

class DetailsViewController: UIViewController {
  
  var selectedRocket:FormattedRocket? = nil
  
  @IBOutlet weak var mainImage: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var successRateLabel: UILabel!
  @IBOutlet weak var successRateImage: UIImageView!
  @IBOutlet weak var isActiveLabel: UILabel!
  @IBOutlet weak var countryLabel: UILabel!
  @IBOutlet weak var firstFlightLabel: UILabel!
  @IBOutlet weak var costPerLaunchLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUIContent()
  }
  
  private func setupUIContent() {
    mainImage.image = selectedRocket?.mainImage
    nameLabel.text = selectedRocket?.name
    successRateLabel.text = selectedRocket?.successRateText
    successRateImage.image = selectedRocket?.successRateBadge
    isActiveLabel.text = selectedRocket?.isActive
    countryLabel.text = selectedRocket?.country
    firstFlightLabel.text = selectedRocket?.firstFlight
    costPerLaunchLabel.text = selectedRocket?.costPerLaunch
    descriptionLabel.text = selectedRocket?.rocketDescription
  }
  
  @IBAction func didPressWikipediaButton(_ sender: UIButton) {
    if let url = selectedRocket?.wikipedia,
       let unwrappedUrl = URL(string: url) {
      UIApplication.shared.open(unwrappedUrl)
    }
  }
}
