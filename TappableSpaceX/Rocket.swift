//
//  Rocket.swift
//  TappableSpaceX
//
//  Created by Stuart Thomas on 20/01/2021.
//

import Foundation

public struct Rocket: Codable {
  public let flickrImages: [String]
  public let name: String
  public let active: Bool
  public let costPerLaunch, successRatePct: Int
  public let firstFlight, country: String
  public let wikipedia: String
  public let rocketDescription: String
  
  enum CodingKeys: String, CodingKey {
    case flickrImages = "flickr_images"
    case name, active
    case costPerLaunch = "cost_per_launch"
    case successRatePct = "success_rate_pct"
    case firstFlight = "first_flight"
    case country, wikipedia
    case rocketDescription = "description"
  }
  
  public init(flickrImages: [String], name: String, active: Bool, stages: Int, costPerLaunch: Int, successRatePct: Int, firstFlight: String, country: String, wikipedia: String, rocketDescription: String) {
    self.flickrImages = flickrImages
    self.name = name
    self.active = active
    self.costPerLaunch = costPerLaunch
    self.successRatePct = successRatePct
    self.firstFlight = firstFlight
    self.country = country
    self.wikipedia = wikipedia
    self.rocketDescription = rocketDescription
  }
}
