//
//  DataService.swift
//  TappableSpaceX
//
//  Created by Stuart Thomas on 20/01/2021.
//

import Foundation
import Alamofire

struct DataService {
  
  private var rocketUrl = "https://api.spacexdata.com/v4/rockets"
  
  func requestFetchRocket(completion: @escaping ([Rocket]?, Error?) -> ()) {
    
    AF.request(rocketUrl).response { response in
      guard let data = response.data else { return }
      do {
        let decoder = JSONDecoder()
        let rocketRequest = try decoder.decode([Rocket].self, from: data)
        completion(rocketRequest, nil)
      } catch let error {
        print(error)
        completion(nil, error)
      }
    }
  }
}
