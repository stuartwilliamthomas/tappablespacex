//
//  RocketViewModel.swift
//  TappableSpaceX
//
//  Created by Stuart Thomas on 21/01/2021.
//

import Foundation
import UIKit

class RocketViewModel {
  
  private var rockets: [Rocket]? {
    didSet {
      guard let r = rockets else { return }
      self.setupRocketDetails(with: r)
      self.didFinishFetch?()
    }
  }
  
  var error: Error? {
      didSet { self.showAlert?() }
  }
  
  var isLoading: Bool = false {
      didSet { self.changeLoadingStatus?() }
  }
  
  var formattedRockets = [FormattedRocket]()
  
  private var dataService: DataService?
  var mainImage: UIImage?
  var name: String?
  var isActive: String?
  var costPerLaunch, successRatePct: String?
  var firstFlight, country: String?
  var wikipedia: URL?
  var rocketDescription: String?
  
  var didFinishFetch: (() -> ())?
  var showAlert: (() -> ())?
  var changeLoadingStatus: (() -> ())?
  
  init(dataService: DataService) {
    self.dataService = dataService
  }
  
  func fetchRockets() {
    self.dataService?.requestFetchRocket(completion: { (rockets, error) in
      if let error = error {
          self.error = error
          self.isLoading = false
          return
      }
      self.isLoading = false
      self.error = nil
      self.rockets = rockets
    })
  }
  
  private func setupRocketDetails(with rockets: [Rocket]?) {
    if let rockets = rockets {
      for items in rockets {
        let newFormattedRocket = FormattedRocket(mainImage: firstImage(rocket: items),
                                                 name: items.name,
                                                 isActive: items.active ? "Active" : "Inactive",
                                                 costPerLaunch: formatCost(cost: items.costPerLaunch),
                                                 successRateText: "Success Rate - ",
                                                 successRateBadge: successRateBadge(rocket: items),
                                                 firstFlight: formatDate(dateString: items.firstFlight),
                                                 country: "Country - \(items.country)",
                                                 wikipedia: items.wikipedia,
                                                 rocketDescription: items.rocketDescription)
        formattedRockets.append(newFormattedRocket)
      }
      let coreDataHelper = CoreDataHelper()
      coreDataHelper.clearAllRockets()
      coreDataHelper.saveNewRocket(newFormattedRockets: formattedRockets)
    }
  }
  
  private func firstImage(rocket: Rocket) -> UIImage? {
    let url = URL(string: rocket.flickrImages.first!)
    let data = try? Data(contentsOf: url!)
    return UIImage(data: data!)
  }
  
  private func formatCost(cost: Int) -> String {
    let numberFormatter = NumberFormatter()
    numberFormatter.groupingSeparator = ","
    numberFormatter.groupingSize = 3
    numberFormatter.usesGroupingSeparator = true
    numberFormatter.decimalSeparator = "."
    numberFormatter.numberStyle = .decimal
    numberFormatter.currencySymbol = "$"
    numberFormatter.maximumFractionDigits = 2
    numberFormatter.locale = Locale(identifier: "en_US")
    numberFormatter.numberStyle = .currencyAccounting
    guard let formattedCost = numberFormatter.string(from: cost as NSNumber) else { return "No recorded Cost" }
    return "Launch Cost - \(formattedCost)"
  }
  
  private func formatDate(dateString: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy'-'MM'-'dd'"
    guard let date = dateFormatter.date(from: dateString) else { return "No Recorded Date" }
    
    
    dateFormatter.dateFormat = "d MMM y"
    let formattedString = dateFormatter.string(from: date)
    return "First flight - \(formattedString)"
  }
  
  private func successRateBadge(rocket: Rocket) -> UIImage? {
    switch rocket.successRatePct {
    case 0..<30:
      return UIImage(named: "red-circle-64")
    case 30..<60:
      return UIImage(named: "orange-circle-64")
    case 60..<101:
      return UIImage(named: "green-circle-64")
    default:
      return UIImage(named: "red-circle-64")
    }
  }
}

struct FormattedRocket {
  var mainImage: UIImage?
  var name: String?
  var isActive: String?
  var costPerLaunch, successRateText: String?
  var successRateBadge: UIImage?
  var firstFlight, country: String?
  var wikipedia: String?
  var rocketDescription: String?
}
