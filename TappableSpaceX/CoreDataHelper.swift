//
//  CoreDataHelper.swift
//  TappableSpaceX
//
//  Created by Stuart Thomas on 24/01/2021.
//

import Foundation
import CoreData
import UIKit

class CoreDataHelper {
  
  private var context:NSManagedObjectContext?
  
  init() {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    context = appDelegate.persistentContainer.viewContext
  }
  
  func clearAllRockets() {
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedRocket")
      let result = try? context?.fetch(fetchRequest)
      let resultData = result as! [NSManagedObject]
      for rocketObject in resultData {
        context?.delete(rocketObject)
      }
    save()
  }
  
  func saveNewRocket(newFormattedRockets: [FormattedRocket]) {
    if let context = context {
      let entity = NSEntityDescription.entity(forEntityName: "SavedRocket", in: context)
      for items in newFormattedRockets {
        let newSavedRocket = NSManagedObject(entity: entity!, insertInto: context)
        
        let mainImageAsData = items.mainImage?.pngData()
        let successRateBadgeAsData = items.successRateBadge?.pngData()
        
        newSavedRocket.setValue(mainImageAsData, forKey: "mainImage")
        newSavedRocket.setValue(items.name, forKey: "name")
        newSavedRocket.setValue(items.isActive, forKey: "isActive")
        newSavedRocket.setValue(items.costPerLaunch, forKey: "costPerLaunch")
        newSavedRocket.setValue(items.successRateText, forKey: "successRateText")
        newSavedRocket.setValue(successRateBadgeAsData, forKey: "successRateBadge")
        newSavedRocket.setValue(items.firstFlight, forKey: "firstFlight")
        newSavedRocket.setValue(items.country, forKey: "country")
        newSavedRocket.setValue(items.wikipedia, forKey: "wikipedia")
        newSavedRocket.setValue(items.rocketDescription, forKey: "rocketDescription")
        
        save()
      }
    }
  }
  
  func retrieve() -> [FormattedRocket]? {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedRocket")
    request.returnsObjectsAsFaults = false
    var retrievedRocketArray = [FormattedRocket]()
    do {
      let result = try context.fetch(request)
      for data in result as! [NSManagedObject] {
        let mainImageAsUIImage = UIImage(data: data.value(forKey: "mainImage") as! Data)
        let successRateBadgeAsUIImage = UIImage(data: data.value(forKey: "successRateBadge") as! Data)
        let retrievedRocket = FormattedRocket(mainImage: mainImageAsUIImage,
                                              name: data.value(forKey: "name") as? String,
                                              isActive: data.value(forKey: "isActive") as? String,
                                              costPerLaunch: data.value(forKey: "costPerLaunch") as? String,
                                              successRateText: data.value(forKey: "successRateText") as? String,
                                              successRateBadge: successRateBadgeAsUIImage,
                                              firstFlight: data.value(forKey: "firstFlight") as? String,
                                              country: data.value(forKey: "country") as? String,
                                              wikipedia: data.value(forKey: "wikipedia") as? String,
                                              rocketDescription: data.value(forKey: "rocketDescription") as? String)
        retrievedRocketArray.append(retrievedRocket)
      }
      return retrievedRocketArray
    } catch {
      print("Retreive Failed")
      return nil
    }
  }
  
  private func save() {
    do {
      try context?.save()
    } catch let error as NSError  {
      print("Could not save \(error), \(error.userInfo)")
    }
  }
}
